import { AuthService } from './../_services/auth.service';
import { Message } from './../_models/message';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { AlertifyService } from '../_services/alertify.service';
import { UserService } from '../_services/user.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

@Injectable()
export class MessagesResolver implements Resolve<Message[]> {
  pageNumber = 1;
  pageSize = 5;
  messageContainer = 'Unread';
  constructor(private _userService: UserService,
    private authService: AuthService,
    private _router: Router,
    private _alertify: AlertifyService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Message[]> {
    return this._userService.getMessages(this.authService.decodedToken.nameid,
       this.pageNumber, this.pageSize, this.messageContainer).pipe(
      catchError(error => {
        this._alertify.error('Problem retrieving messages');
        this._router.navigate(['/home']);
        return of(null);
      })
    );
  }
}
