import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { AlertifyService } from './../_services/alertify.service';
import { UserService } from './../_services/user.service';
import { User } from './../_models/user';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

@Injectable()
export class MemberDetailResolver implements Resolve<User> {

  constructor (private _userService: UserService, private _router: Router,
     private _alertify: AlertifyService) {}

     resolve(route: ActivatedRouteSnapshot): Observable<User> {
       return this._userService.getUser(route.params['id']).pipe(
         catchError(error => {
           this._alertify.error('Problem retrieving data');
           this._router.navigate(['/members']);
           return of(null);
         })
       );
     }
}
