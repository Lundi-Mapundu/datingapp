import { AuthService } from './../../_services/auth.service';
import { UserService } from './../../_services/user.service';
import { AlertifyService } from './../../_services/alertify.service';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/_models/user';
import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {
  @ViewChild('editForm') editForm: NgForm;
  user: User;
  photoUrl: string;
  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (this.editForm.dirty) {
      $event.returnValue = true;
    }
  }
  constructor(
    private _route: ActivatedRoute,
    private _alertify: AlertifyService,
    private _userService: UserService,
    private _authService: AuthService
  ) { }

  ngOnInit() {
    this._route.data.subscribe(data => {
      this.user = data['user'];
    });

    this._authService.currentPhotoUrl.subscribe(photoUrl => this.photoUrl = photoUrl);
  }
  updateUser() {
    this._userService
      .updateUser(this._authService.decodedToken.nameid, this.user)
      .subscribe(next => {
        this._alertify.success('Profile Updated Successfully');
        this.editForm.reset(this.user);
      }, error => {
        this._alertify.error(error);
      });
  }

  updateMainPhoto(photoUrl: string) {
    this.user.photoUrl = photoUrl;
  }
}
